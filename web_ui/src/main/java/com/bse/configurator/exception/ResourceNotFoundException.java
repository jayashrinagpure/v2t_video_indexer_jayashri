package com.bse.configurator.exception;

/**
 * 
 * @author Bharat.Pattani
 *To find if resource does not exists.
 */
public class ResourceNotFoundException extends RuntimeException {

    private Long resourceId;

    /**
     * To find if resource does not exists for given resource id.
     * @param resourceId
     * @param message
     */
    public ResourceNotFoundException(Long resourceId, String message) {
        super(message);
        this.resourceId = resourceId;
    }
}

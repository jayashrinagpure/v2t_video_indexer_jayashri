package com.bse.configurator.exception;

import java.util.List;

/**
 * 
 * @author Bharat.Pattani
 * Class of ExceptionResponse.
 * fields:
 * errorCode[String]
 * errorMessage[String]
 * errors[list]
 */
public class ExceptionResponse {

	private String errorCode;
	private String errorMessage;
	private List<String> errors;

	public ExceptionResponse() {
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
}

package com.bse.configurator.exception;

/**
 * 
 * @author Bharat.Pattani
 *To find if duplicate resource exists.
 */
public class DuplicateFoundException extends RuntimeException {
    private Long resourceId;

    /**
     * To find duplicate of given resource id.
     * @param resourceId long value of resource id
     * @param message corresponding message.
     */
    public DuplicateFoundException(Long resourceId, String message) {
        super(message);
        this.resourceId = resourceId;
    }
}

package com.bse.configurator.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 
 * @author Bharat.Pattani
 * class ExceptionHandlingController
 *
 */
@ControllerAdvice
public class ExceptionHandlingController {

	/**
	 * To know if resource exists on not.
	 * @param ex object of ResourceNotFoundException
	 * @return ResponseEntity with status and message related to exception.
	 */
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<ExceptionResponse> resourceNotFound(ResourceNotFoundException ex) {
		ExceptionResponse response = new ExceptionResponse();
		response.setErrorCode("Not Found");
		response.setErrorMessage(ex.getMessage());

		return new ResponseEntity<ExceptionResponse>(response, HttpStatus.NOT_FOUND);
	}

	/**
	 * To know if valid arguments are passed .
	 * @param ex object of MethodArgumentNotValidException.
	 * @return ResponseEntity with status and message related to exception.
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ExceptionResponse> invalidInput(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		ExceptionResponse response = new ExceptionResponse();
		response.setErrorCode("Validation Error");
		response.setErrorMessage("Invalid inputs.");
		response.setErrors(ValidationUtil.fromBindingErrors(result));

		return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * To know if duplicates are found .
	 * @param ex object of DuplicateFoundException.
	 * @return ResponseEntity with status and message related to exception.
	 */
	@ExceptionHandler(DuplicateFoundException.class)
	public ResponseEntity<ExceptionResponse> duplicateFound(DuplicateFoundException ex) {
		ExceptionResponse response = new ExceptionResponse();
		response.setErrorCode("Duplicate entries found.");
		response.setErrorMessage(ex.getMessage());
		return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
	}
}
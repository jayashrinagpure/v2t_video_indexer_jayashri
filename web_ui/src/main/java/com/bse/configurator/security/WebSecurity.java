package com.bse.configurator.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.bse.configurator.user.ApplicationUserAuditRepository;

import org.springframework.context.annotation.Bean;

import static com.bse.configurator.security.SecurityConstants.SIGN_UP_URL;

import java.util.Arrays;

/**
 * 
 * @author Bharat.Pattani
 * Class of WebSecurity
 * Fields:
 * userDetailsService[UserDetailsService]
 * bCryptPasswordEncoder[BCryptPasswordEncoder]
 * applicationUserAuditRepository[ApplicationUserAuditRepository]
 */
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {
	private UserDetailsService userDetailsService;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private ApplicationUserAuditRepository applicationUserAuditRepository;
	public WebSecurity(ApplicationUserAuditRepository applicationUserAuditRepository,UserDetailsService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userDetailsService = userDetailsService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.applicationUserAuditRepository = applicationUserAuditRepository;
	}

	/**
	 * To allow urls specified in below way.
	 * default allowed are /login, /index.
	 * To provide filters so that authentication and authorization can be done.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().authorizeRequests()
				.antMatchers("/resources/**", "/index.html", "/login.html", "/partials/**", "/", "/error/**","/scripts/**","/css/**","/images/**","/videos/**","/favicon.ico","/fonts/**")
				.permitAll().antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll().anyRequest().authenticated().and()
				.addFilter(new JWTAuthenticationFilter(authenticationManager(),this.applicationUserAuditRepository))
				.addFilter(new JWTAuthorizationFilter(authenticationManager()))
				// this disables session creation on Spring Security
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	/**
	 * To encode password 
	 */
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
	}

	/**
	 * To allow below mentions request and response methods.
	 * @return
	 */
	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration corsConfiguration = new CorsConfiguration().applyPermitDefaultValues();
		corsConfiguration.setAllowedMethods(Arrays.asList("PUT", "DELETE", "GET", "POST"));
		source.registerCorsConfiguration("/**", corsConfiguration);
		return source;
	}
}
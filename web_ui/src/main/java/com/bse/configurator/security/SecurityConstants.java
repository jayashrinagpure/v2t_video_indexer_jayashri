package com.bse.configurator.security;

/**
 * 
 * @author Bharat.Pattani
 *
 */
public class SecurityConstants {
	/** The Constant SECRET. */
    public static final String SECRET = "SecretKeyToGenJWTs";
    
    /** The Constant EXPIRATION_TIME. */
    public static final long EXPIRATION_TIME = 864000000; // 10 days
    
    /** The Constant TOKEN_PREFIX. */
    public static final String TOKEN_PREFIX = "Bearer ";
    
    /** The Constant HEADER_STRING. */
    public static final String HEADER_STRING = "Authorization";
    
    /** The Constant EXPOSE_HEADER_STRING. */
    public static final String EXPOSE_HEADER_STRING="Access-Control-Expose-Headers";
    
    /** The Constant EXPOSE_ROLE. */
    public static final String EXPOSE_ROLE="role";

    /** The Constant SIGN_UP_URL. */
    public static final String SIGN_UP_URL = "/users/sign-up";
}
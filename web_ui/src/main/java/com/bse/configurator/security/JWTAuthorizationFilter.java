package com.bse.configurator.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import static com.bse.configurator.security.SecurityConstants.HEADER_STRING;
import static com.bse.configurator.security.SecurityConstants.SECRET;
import static com.bse.configurator.security.SecurityConstants.TOKEN_PREFIX;

/**
 * 
 * @author Bharat.Pattani
 * Class of JWTAuthorizationFilter.
 * To authorize user.
 */
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	public JWTAuthorizationFilter(AuthenticationManager authManager) {
		super(authManager);
	}

	/**
	 * On every request headers are taken and token is retrieved.
	 * if it is valid then authentication is set.
	 * esle user will ne unauthorized.
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		try {
			String header = req.getHeader(HEADER_STRING);
			if (header == null || !header.startsWith(TOKEN_PREFIX)) {
				chain.doFilter(req, res);
				return;
			}

			UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			chain.doFilter(req, res);
		} catch (Exception e) {
			res.setStatus(HttpStatus.UNAUTHORIZED.value());
		}

	}

	/**
	 * To retrieve role, token and user name from headers.
	 * @param request object of HttpServletRequest
	 * @return
	 */
	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HEADER_STRING);
		if (token != null) {
			// parse the token.
			String user = Jwts.parser().setSigningKey(SECRET.getBytes()).parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
					.getBody().getSubject();
            Jws<Claims> claims = Jwts.parser().setSigningKey(SECRET.getBytes()).parseClaimsJws(token.replace(TOKEN_PREFIX, ""));
            String role = claims.getBody().getAudience().replaceAll("[^a-zA-Z0-9 ,]", "");
			if (user != null) {
                Collection<GrantedAuthority> grantedAuthority = new ArrayList<>();
                GrantedAuthority authority = new SimpleGrantedAuthority(role);
                grantedAuthority.add(authority);
				return new UsernamePasswordAuthenticationToken(user, null,grantedAuthority);
			}
			return null;
		}
		return null;
	}
}
package com.bse.configurator.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * 
 * @author Bharat.Pattani
 *
 */
public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
    ApplicationUser findByUsername(String username);
    
    @Query(nativeQuery=true,value="select email from application_user where COALESCE(email, '') <> ''")
    List<String> findAllEmails();
}
package com.bse.configurator.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Bharat.Pattani
 *
 */
@Repository
public interface ApplicationUserAuditRepository extends JpaRepository<ApplicationUserAudit, Long> {
	/**
	 * To find user by its name.
	 * @param username
	 * @return object of applicationUserAudit.
	 */
	ApplicationUserAudit findByUsername(String username);

}

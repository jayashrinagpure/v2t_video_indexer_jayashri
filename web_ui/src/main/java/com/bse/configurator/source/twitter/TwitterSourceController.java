package com.bse.configurator.source.twitter;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bse.configurator.email.Email;
import com.bse.configurator.email.EmailService;
import com.bse.configurator.exception.DuplicateFoundException;
import com.bse.configurator.exception.ResourceNotFoundException;
import com.bse.configurator.request.type.RequestType;
import com.bse.configurator.request.type.RequestTypeRepository;
import com.bse.configurator.requests.Request;
import com.bse.configurator.requests.RequestOperation;
import com.bse.configurator.requests.RequestRepository;
import com.bse.configurator.source.SourceRepository;
import com.bse.configurator.source.SourceType;
import com.bse.configurator.user.ApplicationUserRepository;
import com.bse.configurator.util.Constants;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Bharat.Pattani
 * Twiiter source controller to perform all oprations needed.
 */
@RestController
@RequestMapping("/source/twitter")
public class TwitterSourceController {
	private TwitterSourceRepository twitterSourceRepository;
	private SourceType hashtagSource, handleSource;
	private RequestRepository requestRepository;
	private RequestTypeRepository requestTypeRepository;

	@Autowired
	EmailService emailService;
	
	private Email email;

	@Autowired
	public TwitterSourceController(TwitterSourceRepository twitterSourceRepository, SourceRepository sourceRepository,RequestRepository requestRepository,
			RequestTypeRepository requestTypeRepository,ApplicationUserRepository applicationUserRepository) {
		this.twitterSourceRepository = twitterSourceRepository;
		this.requestRepository = requestRepository;
		this.requestTypeRepository = requestTypeRepository;
		this.hashtagSource = sourceRepository.findBySourceType("hashtag", "twitter");
		this.handleSource = sourceRepository.findBySourceType("handle", "twitter");
		email = new Email();
		email.setTo(applicationUserRepository.findAllEmails());
	}

	/**
	 * To find twitter source name.
	 * @param sourceTypeName string value of source type.
	 * @return twitter source name base on sourceTypeName
	 */
	public SourceType getSourceType(String sourceTypeName) {
		SourceType sourceType = null;
		switch (sourceTypeName) {
		case "hashtag":
			sourceType = this.hashtagSource;
			break;
		case "handle":
			sourceType = this.handleSource;
			break;
		}
		return sourceType;
	}

	/**
	 * To save twitter source if creator and approver are different.
	 * @param source object of twitterSource.
	 * @param approvedBy user name of approver.
	 * @param createdBy user name of creator.
	 */
	public void saveTwitterSource(TwitterSource source, String approvedBy, String createdBy) {
		source.setApprovedBy(approvedBy);
		source.setRequestedBy(createdBy);
		twitterSourceRepository.save(source);
	}
	
	/**
	 * To save twitter source if creator and approver are same.
	 * @param source object of twitterSource.
	 * @param createdBy user name.
	 */
	public void saveTwitterSource(TwitterSource source, String createdBy) {
		source.setApprovedBy(createdBy);
		source.setRequestedBy(createdBy);
		twitterSourceRepository.save(source);
	}

	/**
	 * To approve twitter source request.
	 * @param existingSource object of twitter source.
	 * @param newSource object of twitter source.
	 * @param approvedBy user name of approver.
	 * @param createdBy user name of creator.
	 */
	public void approveTwitterSourceRequest(TwitterSource existingSource, TwitterSource newSource, String approvedBy,
			String createdBy) {
		if (existingSource == null) {
			throw new ResourceNotFoundException((long) 400, "Source not found.");
		} else {
			existingSource.setEndDate(new Date());
			this.twitterSourceRepository.save(existingSource);
			this.saveTwitterSource(newSource, approvedBy, createdBy);
		}
	}
	
	/**
	 * To add twitter source.
	 * @param source object of twitter source.
	 */
	@PostMapping
	public void addSource(@Valid @RequestBody TwitterSource source) {
		boolean authorized = false;
		boolean isCreatorSuperUser = false;
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		SourceType currentSourceType = this.getSourceType(source.getSourceTypeName());
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (authorized) {
			if (this.twitterSourceRepository.doesSourceExists(currentSourceType, source.getSourceName()) != null) {
				throw new DuplicateFoundException((long) 400, "Duplicate entry found for twitter source name.");
			} else {
				if (source.getRequestedBy() == null) {
					source.setRequestedBy(username);
				}
				if (source.getRequestedDate() == null) {
					source.setRequestedDate(new Date());
					source.setApproveDate(source.getStartDate());
					isCreatorSuperUser = true;
				} else {
					source.setRequestedDate(source.getRequestedDate());
				}
				source.setSourceType(currentSourceType);
				this.saveTwitterSource(source, username, source.getRequestedBy());
				if(isCreatorSuperUser == true) {
					String requestedValue = source.getSourceName() + " | "+source.getdisplayName();
				RequestType requestType = this.requestTypeRepository.findBySourceType(currentSourceType.getSourceType(),currentSourceType.getSourceMedia());
				Request request = new Request(requestType, requestedValue,source.getSourceName(), username, username,new Date(),new Date(),
						RequestOperation.ADD);
				this.requestRepository.save(request);
				}
				this.email.setMessage(String.format("%s has added '%s' twitter %s at %s.", username,
						source.getSourceName(), source.getSourceTypeName(), source.getStartDate()));
				this.emailService.send(this.email);
			}
		} 
		else {
			if (this.twitterSourceRepository.doesSourceExists(currentSourceType, source.getSourceName()) != null) {
				throw new DuplicateFoundException((long) 400, "Duplicate entry found for twitter source name.");
			} else {
				String requestedValue = source.getSourceName() + " | "+source.getdisplayName();
				RequestType requestType = this.requestTypeRepository.findBySourceType(currentSourceType.getSourceType(),
						currentSourceType.getSourceMedia());
				Date requestedDate = new Date();
				Request request = new Request(requestType, requestedValue,source.getSourceName(), username, requestedDate,RequestOperation.ADD);// for other user
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s requested to add '%s' twitter %s at %s", username,
						source.getSourceName(), source.getSourceTypeName(), source.getStartDate()));
				this.emailService.send(this.email);
			}
		}
		}

	/**
	 * To de-active twitter source.
	 * @param source object of twitter source.
	 * @param id long value to identify twitter source.
	 */
	@PutMapping("/inactive/{id}")
	public void deActivateSource(@RequestBody TwitterSource source, @PathVariable("id") Long id) {
		boolean authorized = false;
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		SourceType currentSourceType = this.getSourceType(source.getSourceTypeName());
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		TwitterSource twitterSource = this.twitterSourceRepository.findByActiveSource(id, currentSourceType,
				source.getSourceName());
		if (authorized) {
			if (twitterSource == null) {
				throw new ResourceNotFoundException((long) 400, "Twitter source not found.");
			} else {
				twitterSource.setSourceTypeName(source.getSourceTypeName());
				twitterSource.setEndDate(new Date());
				this.twitterSourceRepository.save(twitterSource);
				TwitterSource updatedTwitterSource = new TwitterSource(twitterSource.getSourceName(),twitterSource.getdisplayName(),
						twitterSource.getSourceTypeName(), false);
				updatedTwitterSource.setSourceType(currentSourceType);
				updatedTwitterSource.setRequestedBy(username);
				updatedTwitterSource.setRequestedDate(new Date());
				updatedTwitterSource.setApproveDate(source.getStartDate());
				this.saveTwitterSource(updatedTwitterSource, username);
				RequestType requestType = this.requestTypeRepository.findBySourceType(currentSourceType.getSourceType(),currentSourceType.getSourceMedia());
				String requestedValue = source.getSourceName()+ " | "+source.getdisplayName();
				Request request = new Request(requestType, requestedValue,twitterSource.getSourceName(), username, username,new Date(),new Date(),
						RequestOperation.INACTIVE);
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s has deactivated '%s' twitter %s at %s.", username,
						source.getSourceName(),source.getSourceTypeName(), source.getStartDate()));
				this.emailService.send(this.email);
			}

		} else {//other user
			if (twitterSource == null) {
				throw new ResourceNotFoundException((long) 400, "Twitter source not found.");
			} else {
				String requestedValue = source.getSourceName()+ " | "+source.getdisplayName();
				RequestType requestType = this.requestTypeRepository.findBySourceType(currentSourceType.getSourceType(),
						currentSourceType.getSourceMedia());
				Date requestedDate = new Date();
				Request request = new Request(requestType, requestedValue, source.getSourceName(), username, requestedDate,RequestOperation.INACTIVE);
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s requested to deactivate '%s' twitter %s at %s.", username,
						source.getSourceName(), source.getSourceTypeName(), source.getStartDate()));
				this.emailService.send(this.email);
			}
		}}

	/**
	 * To activate twitter source.
	 * @param source object of twitter source.
	 * @param id long value to identify twitter source record.
	 */
	@PutMapping("/active/{id}")
	public void activateSource(@RequestBody TwitterSource source, @PathVariable("id") Long id) {
		boolean authorized = false;
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		SourceType currentSourceType = this.getSourceType(source.getSourceTypeName());
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		TwitterSource twitterSource = this.twitterSourceRepository.findByInactiveSource(id, currentSourceType,
				source.getSourceName());
		if (authorized) {
			if (twitterSource == null) {
				throw new ResourceNotFoundException((long) 400, "Twitter source not found.");
			} else {
				twitterSource.setSourceTypeName(source.getSourceTypeName());
				twitterSource.setEndDate(new Date());
				this.twitterSourceRepository.save(twitterSource);
				TwitterSource updatedTwitterSource = new TwitterSource(twitterSource.getSourceName(),twitterSource.getdisplayName(),
						twitterSource.getSourceTypeName(), true);
				updatedTwitterSource.setRequestedBy(username);
				updatedTwitterSource.setRequestedDate(new Date());
				updatedTwitterSource.setApproveDate(source.getStartDate());
				updatedTwitterSource.setSourceType(currentSourceType);
				RequestType requestType = this.requestTypeRepository.findBySourceType(currentSourceType.getSourceType(),currentSourceType.getSourceMedia());
				String requestedValue = source.getSourceName() + " | "+source.getdisplayName();
				Request request = new Request(requestType, requestedValue,twitterSource.getSourceName(), username, username,new Date(),new Date(),
						RequestOperation.ACTIVE);
				this.requestRepository.save(request);
				this.saveTwitterSource(updatedTwitterSource, username);
				this.email.setMessage(String.format("%s has activated '%s' twitter %s at %s.", username,
						source.getSourceName(), source.getSourceTypeName(), source.getStartDate()));
				this.emailService.send(this.email);
			}
		} else {
			if (twitterSource == null) {
				throw new ResourceNotFoundException((long) 400, "Twitter source not found.");
			} else {
				String requestedValue = source.getSourceName() + " | "+source.getdisplayName();
				RequestType requestType = this.requestTypeRepository.findBySourceType(currentSourceType.getSourceType(),
						currentSourceType.getSourceMedia());
				Date requestedDate = new Date();
				Request request = new Request(requestType, requestedValue, source.getSourceName(), username, requestedDate,RequestOperation.ACTIVE);
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s requested to activate '%s' twitter %s at %s.", username,
						source.getSourceName(), source.getSourceTypeName(), source.getStartDate()));
				this.emailService.send(this.email);
			}
		}
	}
	
	/**
	 * To get list of all twitter source records.
	 * @return all twitter source records.
	 */
	@GetMapping
	public List<TwitterSource> getSources() {
		return this.twitterSourceRepository.findAllRecords();
	}
	
	/**
	 * To get records based on status.
	 * @param status value as inactive/active
	 * @return twitter source records by status as true/false.
	 */
	@GetMapping("/{status:active|inactive}")
	public List<TwitterSource> getSourcesByStatus(@PathVariable("status") String status) {
		switch(status) {
		case "active":
			return this.twitterSourceRepository.findRecordsByStatus(true);
		case "inactive":
			return this.twitterSourceRepository.findRecordsByStatus(false);
		}
		return null;
	}

}

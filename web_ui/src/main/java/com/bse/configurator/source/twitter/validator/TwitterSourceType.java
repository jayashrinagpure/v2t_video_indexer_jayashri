package com.bse.configurator.source.twitter.validator;

import javax.validation.Constraint;
import javax.validation.Payload;

import java.lang.annotation.*;

/**
 * 
 * @author Bharat.Pattani
 * To know if twitter source type is valid or not.
 */
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {TwitterSourceTypeValidator.class})
public @interface TwitterSourceType {
	String message() default "Invalid Source Type";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}

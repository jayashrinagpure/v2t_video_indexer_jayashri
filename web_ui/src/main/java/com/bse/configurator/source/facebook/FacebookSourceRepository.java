package com.bse.configurator.source.facebook;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bse.configurator.source.SourceType;
/**
 * 
 * @author Bharta.Pattani
 *
 */
@Repository
public interface FacebookSourceRepository extends JpaRepository<FacebookSource, Long>{
	/**
	 * To get all records of facebook source.
	 * @return All records of facebook source.
	 */
	@Query(nativeQuery=true,value="SELECT CASE WHEN t2.existing_value IS NULL THEN false ELSE true END as requested,t1.* FROM (SELECT s.source_type as source_type,fb.* FROM public.facebook_source fb INNER JOIN source_type s ON fb.facebook_source_type_id = s.id) t1 LEFT OUTER JOIN (SELECT rt.request_media,rt.request_type,r.* from public.request r INNER JOIN public.request_type rt ON r.request_type_id = rt.id where rt.request_media='facebook' and rt.request_type in ('page','user','group') and r.request_status='PENDING') t2 ON t1.facebook_source_name=t2.existing_value AND t1.source_type=t2.request_type where t1.end_date='9999-12-31 23:59:59'")
	List<FacebookSource> findAllRecords();
	
	/**
	 * To find records based on status.
	 * @param status boolean value as true/false
	 * @return facebook source records based on status.
	 */
	@Query(nativeQuery=true,value="SELECT CASE WHEN t2.existing_value IS NULL THEN false ELSE true END as requested,t1.* FROM (SELECT s.source_type as source_type,fb.* FROM public.facebook_source fb INNER JOIN source_type s ON fb.facebook_source_type_id = s.id) t1 LEFT OUTER JOIN (SELECT rt.request_media,rt.request_type,r.* from public.request r INNER JOIN public.request_type rt ON r.request_type_id = rt.id where rt.request_media='facebook' and rt.request_type in ('page','user','group') and r.request_status='PENDING') t2 ON t1.facebook_source_name=t2.existing_value AND t1.source_type=t2.request_type where t1.end_date='9999-12-31 23:59:59' and t1.facebook_source_status = ?1")
	List<FacebookSource> findRecordsByStatus(Boolean status);
	
	/**
	 * To find record based on id.
	 * @param id long value to identify record.
	 * @return facebook source record with given id.
	 */
	@Query("select t from FacebookSource t where t.endDate='9999-12-31 23:59:59' and  t.id = ?1")
	List<FacebookSource> findById(long id);
	
	/**
	 * To find if source exists or not.
	 * @param sourceType type of facebook source.
	 * @param sourceName name of facebook source.
	 * @return facebook source record if it exists.
	 */
	@Query("select t from FacebookSource t where t.endDate='9999-12-31 23:59:59' and t.sourceType=?1 and t.sourceName=?2")
	FacebookSource doesSourceExists(SourceType sourceType,String sourceName);
	
	/**
	 * To find records by active status.
	 * @param id long value to identify record.
	 * @param sourceType type of facebook source.
	 * @param sourceName name of facebook source.
	 * @return facebook source record with active status.
	 */
	@Query("select t from FacebookSource t where t.endDate='9999-12-31 23:59:59' and t.id = ?1 and t.sourceType=?2 and t.status = true and t.sourceName=?3")
	FacebookSource findByActiveSource(Long id,SourceType sourceType,String sourceName);
	
	/**
	 * To find records by inactive status.
	 * @param id long value to identify record.
	 * @param sourceType type of facebook source.
	 * @param sourceName name of facebook source.
	 * @return facebook source record with inactive status.
	 */
	@Query("select t from FacebookSource t where t.endDate='9999-12-31 23:59:59' and t.id = ?1 and t.sourceType=?2 and t.status = false and t.sourceName=?3")
	FacebookSource findByInactiveSource(Long id,SourceType sourceType,String sourceName);
}

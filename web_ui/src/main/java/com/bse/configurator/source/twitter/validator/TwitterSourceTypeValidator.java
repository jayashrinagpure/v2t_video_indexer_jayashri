package com.bse.configurator.source.twitter.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 
 * @author Bharat.Pattani
 * To know if twitter source type is handle or hashtag.
 */
public class TwitterSourceTypeValidator implements ConstraintValidator<TwitterSourceType, String> {

	@Override
	public void initialize(TwitterSourceType constraintAnnotation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isValid(String twitterSourceTypeName, ConstraintValidatorContext context) {
		if(twitterSourceTypeName == null) {
			return false;
		}
		else if(twitterSourceTypeName.equals("hashtag") || twitterSourceTypeName.equals("handle"))
			return true;
		else
			return false;
	}

}

package com.bse.configurator.source.web;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bse.configurator.source.twitter.TwitterSource;
@Repository
public interface WebSourceRepository  extends JpaRepository<WebSource, Long>{
	@Query("select t from WebSource t where t.endDate='9999-12-31 23:59:59'")
	List<TwitterSource> findAllRecords();
}

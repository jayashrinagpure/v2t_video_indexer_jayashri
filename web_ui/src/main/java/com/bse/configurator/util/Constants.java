package com.bse.configurator.util;

public class Constants {
	
	/** The Constant BLANK. */
	public static final String BLANK = "";
	
	/** The Constant COLON. */
	public static final String COLON = " : ";
	
	/** The Constant DASH. */
	public static final String DASH = " - ";
	
	/** The Constant KEY_TXNID. */
	public static final String KEY_TXNID = "TXNID";
	
	/** The Constant XPATH_TXNID. */
	public static final String XPATH_TXNID = "//transcationId";
	
	/** The Constant ENTRY. */
	public static final String ENTRY = "Entry";
	
	/** The Constant EXIT. */
	public static final String EXIT = "Exit";
	
	/**The Constant OTHERUSER. */
	public static final String OTHERUSER = "USER";
	
	/** The Constant GRAM. */
	public static final String GRAM = "company gram";
	
	/** The Constant COMPANY. */
	public static final String COMPANY = "all";
	
	/** The Constant SUPERUSER. */
	public static final String SUPERUSER = "SUPER USER";

}
package com.bse.configurator.requests;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bse.configurator.request.type.RequestType;
/**
 * 
 * @author Pushpa.Gudmalwar
 *
 */
@Repository
public interface RequestRepository extends JpaRepository<Request, Long>{
	/**
	 * To find all requests raised till this moment.
	 * @return all requests raised/approved/rejected.
	 */
	@Query("select t from Request t")
	List<Request> findAllRequest();

	/**
	 * To find request using its id.
	 * @param id request_id of raised request.
	 * @return an object of request with its id.
	 */
	@Query("select t from Request t where  t.id = ?1")
	Request findById(long id);
	
	/**
	 * To find all requests with its status.
	 * @param status value as pending/approved/rejected.
	 * @return list of records based on status.
	 */
	@Query("select t from Request t where  t.requestStatus = ?1")
	List<Request> findRecordsByStatus(RequestStatus status);

	/**
	 * To find if request exist or not.
	 * @param requestType type of request based on source,keywords, company.
	 * @param requestInfoName value of request raised.
	 * @return object of request if it exists.
	 */
	@Query("select t from Request t where  t.requestType=?1  and t.requestedValue=?2")
	Request doesRequestExists(RequestType requestType,String requestInfoName);

}

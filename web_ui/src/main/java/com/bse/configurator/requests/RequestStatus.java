package com.bse.configurator.requests;
/**
 * 
 * @author Pushpa.Gudmalwar
 *
 */
public enum RequestStatus {
	PENDING,
	APPROVED,
	REJECTED
}

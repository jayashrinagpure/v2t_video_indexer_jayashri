package com.bse.configurator.requests;

/**
 * 
 * @author Pushpa.Gudmalwar
 *
 */
public enum RequestOperation {
	ADD,
	ACTIVE,
	INACTIVE,
	UPDATE,
	REMOVE
}

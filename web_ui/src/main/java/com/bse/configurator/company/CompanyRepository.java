package com.bse.configurator.company;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author Bharat.pattani
 *
 */

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long>{
	/**
	 * 
	 * @param mediaType a string, company request
	 * @param requestType a string, to update
	 * @return company records with given meadiaType and requestType
	 */
	@Query(nativeQuery=true,value="SELECT CASE WHEN t2.existing_value IS NULL THEN false ELSE true END as requested,t1.* from public.companies t1 LEFT OUTER JOIN (SELECT rt.request_media,rt.request_type,r.* from public.request r INNER JOIN public.request_type rt ON r.request_type_id = rt.id where rt.request_media=?1 and rt.request_type in (?2) and r.request_status='PENDING') t2 ON t1.company_grams=t2.existing_value where t1.end_date='9999-12-31 23:59:59'")
	List<Company> findAllRecords(String mediaType,String requestType);
	/**
	 * 
	 * @param id
	 * @return active company records by id.
	 */
	@Query("select t from Company t where t.endDate='9999-12-31 23:59:59' and t.status=true and t.id=?1")
	Company findByActiveId(Long id);
	
	/**
	 * 
	 * @param id
	 * @return inactive company records by id.
	 */
	@Query("select t from Company t where t.endDate='9999-12-31 23:59:59' and t.status=false and t.id=?1")
	Company findByInactiveId(Long id);
	
	/**
	 * 
	 * @param status
	 * @return company records based on status.
	 */
	@Query("select t from Company t where t.endDate='9999-12-31 23:59:59' and t.status=?1")
	List<Company> findByStatus(Boolean status);
	
	/**
	 * 
	 * @param id
	 * @return company record by id.
	 */
	@Query("select t from Company t where t.endDate='9999-12-31 23:59:59' and t.id=?1")
	Company findById(Long id);
	

}

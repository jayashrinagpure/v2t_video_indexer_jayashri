package com.bse.configurator.keyword;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.bse.configurator.keyword.Keyword;
/**
 * 
 * @author Pushpa.Gudmalwar
 *
 */
@Repository
public interface KeywordRepository extends JpaRepository<Keyword, Long> {
	/**
	 * 
	 * @param keywordMedia type as in web,facebook, twitter
	 * @return Keywords records based on keywordMedia.
	 */
    @Query(nativeQuery=true,value="SELECT CASE WHEN t2.existing_value IS NULL THEN false ELSE true END as requested,t1.* from public.keywords  t1 LEFT OUTER JOIN (SELECT rt.request_media,rt.request_type,r.* from public.request r INNER JOIN public.request_type rt ON r.request_type_id = rt.id where rt.request_media=LOWER(?1) and rt.request_type in ('rejection','selection') and r.request_status='PENDING') t2 ON t1.keyword=t2.existing_value where t1.end_date='9999-12-31 23:59:59' and t1.keyword_media=?1")
	List<Keyword> findAllRecords(String keywordMedia);
	
    /**
     * To find keywords records based on status.
     * @param status boolean value of each keyword record.
     * @param keywordMedia type as in web,facebook, twitter
     * @return
     */
	@Query(nativeQuery=true,value="SELECT CASE WHEN t2.existing_value IS NULL THEN false ELSE true END as requested,t1.* from public.keywords  t1 LEFT OUTER JOIN (SELECT rt.request_media,rt.request_type,r.* from public.request r INNER JOIN public.request_type rt ON r.request_type_id = rt.id where rt.request_media=LOWER(?2) and rt.request_type in ('rejection','selection') and r.request_status='PENDING') t2 ON t1.keyword=t2.existing_value where t1.end_date='9999-12-31 23:59:59' and t1.keyword_media=?2 and  t1.keyword_status = ?1")
	List<Keyword> findRecordsByStatus(Boolean status,String keywordMedia);
	
	/**
	 * To find if keyword is already existing or not.
	 * @param keyword object of keyword.
	 * @param keywordMedia type of keyword passed.
	 * @return keyword object if exists.
	 */
	@Query("select t from Keyword t where t.endDate='9999-12-31 23:59:59' and  t.keyword=?1 and t.media=?2 ")
	Keyword doesKeywordExists(String keyword,KeywordMedia keywordMedia);
	
	/**
	 * To find keyword by active status.
	 * @param id long value of keyword id.
	 * @param keywordType selection/rejection type
	 * @param keywordMedia media of keyword.
	 * @param keyword object of keyword.
	 * @return returns object of keyword by active status.
	 */
	@Query("select t from Keyword t where t.endDate='9999-12-31 23:59:59' and t.id = ?1 and t.type=?2 and t.status = true and t.media=?3 and t.keyword=?4")
	Keyword findByActive(Long id,KeywordType keywordType,KeywordMedia keywordMedia,String keyword);
	
	/**
	 * To find keyword by inactive status.
	 * @param id long value of keyword id.
	 * @param keywordType selection/rejection type
	 * @param keywordMedia media of keyword.
	 * @param keyword object of keyword.
	 * @return returns object of keyword by inactive status.
	 */
	@Query("select t from Keyword t where t.endDate='9999-12-31 23:59:59' and t.id = ?1 and t.type=?2 and t.status = false and t.media=?3 and t.keyword=?4")
	Keyword findByInactive(Long id,KeywordType keywordType,KeywordMedia keywordMedia,String keyword);
	
}

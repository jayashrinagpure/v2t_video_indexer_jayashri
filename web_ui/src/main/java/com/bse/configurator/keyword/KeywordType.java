package com.bse.configurator.keyword;
/**
 * 
 * @author Bharat.Pattani
 * enum of keyword type.
 */
public enum KeywordType {
SELECTION,
REJECTION
}

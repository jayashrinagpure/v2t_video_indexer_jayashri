/**
 * @author Jayashri Nagpure
 * @name webApp.controller: signupController
 * @description
 * #signupController
 * It is Sign Up controller.
 * This service is responsible for providing Sign Up facility to user.
 * @requires $scope
 * @requires $http
 * @requires Flash
 */

app.controller('signupController', function ($scope,$http,Flash) {
	/**
	 * Sign up
	 * @description - Registering new user.
	 * @param {object} credentials - Credentials enter by user
	 */
	 $scope.signup = function (credentials) {
			var signupCredentials = {
					"username": credentials.username,
					"password": credentials.password
				};
				$http({
					method: "POST",
					url: 'users/sign-up',
					data: signupCredentials
				}).then(function (response) {
					var message = '<strong>Well done!</strong> You successfully signed up.You need approval for login';
					var id = Flash.create('success', message);
				}, function (reason) {
					var message = '<strong>Error</strong> Unable to sign up';
					var id = Flash.create('danger', message);
					$scope.error = reason.data;
				}); 
                $scope.signupCredentials = null;
	 }
});

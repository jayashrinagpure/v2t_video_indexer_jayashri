/**
 * @author Pushpa Gudmalwar
 * @name webApp.controller: resetPasswordController
 * @description
 * #resetPasswordController
 * It is reset password controller.
 * This service is responsible for providing reset password functionality to user.
 * @requires $scope
 * @requires $state
 * 
 * @property {object} email : object stores user.
 */

app.controller('resetPasswordController',function($state,$scope) {
	//resetPassword(email)
	/**
	 * Resets password 
	 * @description - Resets password 
	 * @param {object} passwordCredentials - Password credentials entered by user.
	 */
	 $scope.resetPassword = function (passwordCredentials) {
			var email = {
					"email": passwordCredentials.text
				};
				/*$http({
					method: "POST",
					url: 'users/forgot-password',
					data: email
				}).then(function (response) {
					var message = 'Email has been sent to you .Please check it';
					var id = Flash.create('success', message);
				}, function (reason) {
					var message = 'Something went wrong. Please try again later';
					var id = Flash.create('danger', message);
					$scope.error = reason.data;
				}); */
             $scope.email = null;
	 }
	});
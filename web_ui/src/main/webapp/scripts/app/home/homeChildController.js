/**
 * @author Jayashri Nagpure
 * @name webApp.controller:homeChildController
 * @description
 * #homeChildController
 * Child controller.
 * This controller is responsible for brief details of project.
 * @requires $scope
 * 
 * @property {array} menu : array This holds the page details of the current/selected page.
 */

app.controller('homeChildController',function($scope) {
	$scope.menu.current='home';
	});
/**
 * @author Pushpa Gudmalwar 
 * @name webApp.controller:homeController
 * @description
 * #homeController
 * It is a home controller.
 * Parent controller.
 * This controller is responsible for loading container and heading for page.
 * @requires $scope
 * @requires $State
 * @requires localStorageService
 * @requires AuthenticationService
 * 
 * @property { object } authData : object handles user information
 * @property {array} menu : array This holds the page details of the current/selected page.
 * @property {String} userName: String holds user name.
 */

app.controller('homeController',function($state,$scope,localStorageService,AuthenticationService) {

	$scope.menu = {current:'home'};

		var userName;
		var authData = localStorageService.get('authorizationData');
		if(authData == null){
       	    AuthenticationService.fillAuthData();
       	 $state.transitionTo('login');
        }
        if (authData) {
        	$scope.userName = authData.userName;
        }
	});
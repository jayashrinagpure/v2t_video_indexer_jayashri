/**
 * @author Jayashri Nagpure
 * @name webApp.controller: videototextController
 * @description
 * #videototextController
 * It is a video to text controller.
 * This controller is responsible for showing the details video to text converison.
 * @requires $state
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 */
app.controller('tvtickerController', function($state, $scope, $http,$timeout, $compile, $window, Upload) {
	$scope.menu.current="tvticker";
	var i=0;
	$scope.videoPlay = function(videoNum) {
	    document.getElementById("videoPlayer").setAttribute("src", "videos/"+videoNum + ".mp4");
	    document.getElementById("videoPlayer").load();
	    document.getElementById("videoPlayer").play();
	}
	document.getElementById('videoPlayer').addEventListener('ended', myHandler, false);
	$scope.videoPlay(0); // play the video

	function myHandler() {
		$scope.videoPlay(i);
	        i++;
	}
	  
});
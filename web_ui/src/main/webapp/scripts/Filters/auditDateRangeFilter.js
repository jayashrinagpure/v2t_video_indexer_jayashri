/**
 * @author Jayashri Nagpure
 * @name webApp.filter: auditDateRangeFilter
 * @description
 * #auditDateRangeFilter
 * It is audit date range filter.
 * This filter is responsible for filtering data based on date range.
 */

app.filter("auditDateRangeFilter", function() {
	/**
	 * Spliting date and getting request data according to date ranges.
	 * @description - Spliting input date with (-) and returnign data between to and from dates.
	 * @param {string} input - Input date. 
	 */
	function parseDate(input) {
		  var parts = input.split('-');
		  // Note: months are 0-based
		  return new Date(parts[2], parts[1]-1, parts[0]); 
		}
	  return function(items, from, to) {
		  if (from == '' && to == '')
	        {return items;}
	        var fromDate = parseDate(from);
	        var toDate = parseDate(to);
	       
	        var arrayToReturn = [];    
	        if(items && items.length>0){
		        for (var i=0; i<items.length; i++){		        	
		            var requestedDate = new Date(items[i].requestedDate ),   
		            actionedDate = new Date(items[i].actionedDate );
		            var formatedrequestedDate=new Date(requestedDate.getFullYear(),requestedDate.getMonth(),requestedDate.getDate()),
		            formatedactionedDate = new Date(actionedDate.getFullYear(),actionedDate.getMonth(),actionedDate.getDate());
		            if ((formatedrequestedDate >= fromDate && formatedrequestedDate <= toDate) || (formatedactionedDate >= fromDate && formatedactionedDate <= toDate))  {
		                arrayToReturn.push(items[i]);
		            }
		        }
	        }
	        return arrayToReturn;
	  };
});

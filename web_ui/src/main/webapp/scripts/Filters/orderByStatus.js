/**
 * @author Pushpa Gudmalwar
 * @name webApp.filter: orderByStatus
 * @description
 * #orderByStatus
 * It is order by status filter.
 * This filter is responsible for providing filtered data based on status.
 */

app.filter('orderByStatus', function() {
	/**
	 * Ordering given list by provided status.
	 * @param {object} lists - List. 
	 * @param {string} orderStatus - Status values(active,Inactive).
	 */
	return function(lists, orderStatus) {
		var listInactive = [], listActive = [];
		//separating based on status
		if(!lists)
			return;
		for (var i = 0, j = 0, k = 0; i < lists.length; i++) {
			if (lists[i].status == true) {
				listActive[j++] = lists[i];//active
			} else {
				listInactive[k++] = lists[i];//inactive
			}
		}
		if (orderStatus == 'active') {//status active
			return listActive;
		} else if (orderStatus == 'inactive') {//status inactive
			return listInactive;
		}
		return lists;//status both
	}
});
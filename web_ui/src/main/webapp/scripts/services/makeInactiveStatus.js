/**
 * @author Pushpa Gudmalwar
 * @name webApp.factory: changeInactive
 * @description
 * #changeInactive
 * It is change inactive factory.
 * This factory is responsible for changing status.
 */

app.factory('changeInactive', function() {
	return {
		inactiveStatus : function(input) {
			input.status = false;
		}
	}
});
/**
 * @author Jayashri Nagpure
 * @name webApp.service: getRumoursVideoListService
 * @description
 * #getRumoursVideoListService
 * It is service to get rumours video list
 * This service is responsible for getting processed rumours video list.
 */

app.service('getYoutubeLiveMetadataService', function ($http,$q) {
    this.getMetadata = function() {
        var deferred = $q.defer();
        $http({
                method: 'POST',
                url: 'http://smart-analytica.southcentralus.cloudapp.azure.com:5000/next_chunk'
            })
            .then(function(data){
                deferred.resolve(data);
            },function(data){
            	 errorToasty("Failed to load data");
                 deferred.reject(data)
            });
        return deferred.promise;
    };
});

		

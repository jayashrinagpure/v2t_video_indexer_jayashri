'use strict';

/**
 * @author Pushpa Gudmalwar
 * @name webApp.factory: authInterceptorService
 * @description
 * #authInterceptorService
 * It is Authentication factory.
 * This factory is responsible for providing authentication to user, for every request sent from browser.
 * @requires $rootScope
 * @requires $q
 * @requires localStorageService
 * @requires toasty
 * 
 * @property {object} authInterceptorServiceFactory : object stores request and response information.
 */

app.factory('authInterceptorService', function($q, $rootScope,localStorageService,toasty) {
 
    var authInterceptorServiceFactory = {};
	/**
	 * Setting authorization header 
	 * @description - Setting authorization header and permissions.
	 * @param {object} config - configuration object
	 */
    var _request = function (config) {
 
        config.headers = config.headers || {};
        
        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = authData.token;
            $rootScope.permissions = true;//setting manually until backend is done
        }
 
        return config;
    }
 
	/**
	 * Error response.
	 * @description - Handling response errors with appropriate messages.
	 * @param {object} rejection - Handles error response. 
	 */
    var _responseError = function (rejection) {
        if (rejection.status === 400) {
        	toasty.error({
		        title: 'error',
		        msg: 'Bad Request -The server cannot process the request.',
		        timeout: 1500,
		        showClose: true,
		        clickToClose: true
		    });
        }
        if (rejection.status === 401) {
        	toasty.error({
		        title: 'error',
		        msg: 'Unauthorized-Requested resource requires an authentication.',
		        timeout: 1500,
		        showClose: true,
		        clickToClose: true
		    });
        }
        if (rejection.status === 403) {
        	toasty.error({
		        title: 'error',
		        msg: 'Access Denied-Not allowed to proceed.',
		        timeout: 1500,
		        showClose: true,
		        clickToClose: true
		    });
        }
        if (rejection.status === 404) {
        	toasty.error({
		        title: 'error',
		        msg: 'Resource not found.',
		        timeout: 1500,
		        showClose: true,
		        clickToClose: true
		    });
        }
        if (rejection.status === 500) {
        	toasty.error({
		        title: 'error',
		        msg: 'Webservice currently unavailable.',
		        timeout: 1500,
		        showClose: true,
		        clickToClose: true
		    });
        }
        return $q.reject(rejection);
    }
 
    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;
 
    return authInterceptorServiceFactory;
});